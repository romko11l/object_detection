FROM python:3.12-bookworm

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /object_detection

# Cache dependencies
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6 -y
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

COPY ./ ./

WORKDIR /object_detection/backend

ENTRYPOINT ["python", "main.py"]
