# Локальная сборка и запуск

```bash
# Скачать / обучить модель и положить в ./fine_tuned_model.pth
docker build -t object_detection .
docker-compose up
```
