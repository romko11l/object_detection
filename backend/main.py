from fastapi import FastAPI, File
from fastapi.responses import Response

import torch
import io
from PIL import Image

import torchvision
from torchvision.utils import draw_bounding_boxes, draw_segmentation_masks
import torchvision.transforms.functional as transform
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor
from torchvision.transforms import v2 as T

import uvicorn

app = FastAPI()


def get_model_instance_segmentation(num_classes):
    # load an instance segmentation model pre-trained on COCO
    model = torchvision.models.detection.maskrcnn_resnet50_fpn()

    # get number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    # now get the number of input features for the mask classifier
    in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
    hidden_layer = 256
    # and replace the mask predictor with a new one
    model.roi_heads.mask_predictor = MaskRCNNPredictor(
        in_features_mask,
        hidden_layer,
        num_classes
    )

    return model


def get_transform(train):
    transforms = []
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    transforms.append(T.ToDtype(torch.float, scale=True))
    transforms.append(T.ToPureTensor())
    return T.Compose(transforms)


num_classes = 2
model = get_model_instance_segmentation(num_classes=num_classes)
model.load_state_dict(torch.load("../fine_tuned_model.pth"))

device = "cuda:0" if torch.cuda.is_available() else "cpu"
model = model.to(device)

eval_transform = get_transform(train=False)
model.eval()


def image_to_bytes(image: Image) -> bytes:
    imgByteArr = io.BytesIO()
    image.save(imgByteArr, "png")
    imgByteArr = imgByteArr.getvalue()
    return imgByteArr


def process_image(image: Image) -> Image:
    image = transform.to_tensor(image)

    with torch.no_grad():
        x = eval_transform(image)
        # convert RGBA -> RGB and move to device
        x = x[:3, ...].to(device)
        predictions = model([x, ])
        pred = predictions[0]

    image = (255.0 * (image - image.min()) / (image.max() - image.min())).to(torch.uint8)
    image = image[:3, ...]
    pred_labels = [f"pedestrian: {score:.3f}" for label, score in zip(pred["labels"], pred["scores"])]
    pred_boxes = pred["boxes"].long()
    output_image = draw_bounding_boxes(image, pred_boxes, pred_labels, colors="red")

    masks = (pred["masks"] > 0.7).squeeze(1)
    output_image = draw_segmentation_masks(output_image, masks, alpha=0.5, colors="blue")

    return transform.to_pil_image(output_image)


@app.post("/detect_objects/")
async def detect_objects(file: bytes = File()):
    input_image = Image.open(io.BytesIO(file))
    processed_image = process_image(input_image)
    return Response(
        content=image_to_bytes(processed_image),
        media_type="image/png",
    )


def main():
    uvicorn.run(app, host='0.0.0.0', port=8080)


if __name__ == '__main__':
    main()
